provider "aws" {
  region = "eu-west-3"
}



variable "environment" {}
variable codepipeline_bucket {}

module "cloudfront-s3-website" {
  source  = "HarishKM7/cloudfront-s3-website/aws"
  version = "0.1.0"
}

