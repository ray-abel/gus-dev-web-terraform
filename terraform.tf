terraform {
  backend "s3" {
    bucket = "gus-dev-web-terraform-state-bucket01"
    key    = "dev/terraform.tfstate"
    region = "eu-west-3"
  }
}

